package utils.enums;

public enum RunType {
    HEADLESS("headless"),
    NON_HEADLESS("non_headless");

    public final String lower;

    RunType(String lower) {
        this.lower = lower;
    }
}
