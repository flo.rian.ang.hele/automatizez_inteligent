package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;
import utils.BaseClass;
import utils.Pages;
import utils.SheetsQuickstart;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;

public class AllPagesTests extends BaseClass {

    // link-ul catre paginele web de testat
    public static final String testingLink =
            "https://docs.google.com/spreadsheets/d/1tccP4ahYMU6-vxlaARYHa-LWzsjH6o-3biQHqpQPStc/edit?usp=sharing";

    // pregatim contoarele
    int successCounter = 0;
    int failureCounter = 0;

    // Scenariul de testare #3
    @Test
    public void verifyAllPagesAreWorkingAsExpected() throws GeneralSecurityException, IOException {

        // accesam link-ul cu paginile si extragem paginile
        SheetsQuickstart.getValues();
        List<Object> urls = Pages.urls;

        // verificam fiecare pagina in parte si numaram cate au fost cu succes si cate au picat
        for (Object url : urls) {
            driver.navigate().to(url.toString());

            try {
                Assertions.assertEquals(url, driver.getCurrentUrl());
                take.screenshot(driver);
                successCounter++;
                System.out.println("Success +" + successCounter + " for url: " + url);
            } catch (AssertionFailedError e) {
                take.failureScreenshot(driver);
                failureCounter++;

                System.out.println();
                System.out.println("Failure +" + failureCounter + " at url: " + url);
                System.out.println(e.getMessage());
                System.out.println();
            }
        }
    }
}
