package utils;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class Taking {
    static Logger logger = Logger.getLogger(Taking.class.getName());

    public static String folderPath = "Document Evidence/";
    static String imagePath;

    private static int count = 0;
    private static final int time = 500;

    public static int getCount() {
        return count;
    }

    public void screenshot(WebDriver driver) {
        setImagePathWihError(false);
        createScreenshot(driver);
    }

    public void failureScreenshot(WebDriver driver) {
        setImagePathWihError(true);
        createScreenshot(driver);
    }

    public void aMoment() {
        try {
            TimeUnit.MILLISECONDS.sleep(time);
        } catch (InterruptedException e) {
            throw new RuntimeException("Needed more time");
        }
    }

    // privates
    private static void setImagePathWihError(boolean is) {
        String fileName = "_sc";
        if (is) {
            fileName = "_error_sc";
        }
        imagePath = "/" + fileName + (++count) + ".png";
    }

    private static void createScreenshot(WebDriver driver) {
        File screenshotFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        try {
            // Save the screenshot to a file
            FileUtils.copyFile(screenshotFile, new File(folderPath + imagePath));
//            logger.log(Level.INFO, "Screenshot saved successfully.");
        } catch (Exception e) {
//            logger.log(Level.INFO,"Failed to save screenshot: " + e.getMessage());
        }
    }
}
