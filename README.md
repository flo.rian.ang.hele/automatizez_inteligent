# automatizez_inteligent



## Getting started
git clone the project

run gradle build


## To customize your google cloud account

YT video: 
https://www.youtube.com/watch?v=8yJrQk9ShPg


Steps for enabling google api: 
https://developers.google.com/sheets/api/quickstart/java


Download your json credential key and add content in src/main/resources/credentials.json


### IMPORTANT

Might need to configure the "Client ID for Web application" 
    
In section "Authorized redirect URIs"
    
"Add URI" (http://localhost:8888/Callback)

