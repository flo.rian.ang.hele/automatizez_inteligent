package utils;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import utils.enums.RunType;

import java.time.Duration;
import java.util.logging.Level;
import java.util.logging.Logger;

import static utils.enums.RunType.*;

public class BaseClass {
    public static WebDriver driver;
    public static Logger logger = Logger.getLogger(BaseClass.class.getName());

    public static Taking take;

    // setam tipul rularii
    public static RunType runType = NON_HEADLESS;

    @BeforeEach
    public void setupChrome() {

        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});
        options.addArguments(runType.lower);

        driver = new ChromeDriver(options);
        logger.log(Level.INFO, "Opening chrome browser");
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

        take = new Taking();
    }

    @AfterEach
    public void quitChrome() {
        driver.quit();
        logger.log(Level.INFO, "Closing chrome browser");
    }
}
