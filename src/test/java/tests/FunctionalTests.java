package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import utils.BaseClass;

public class FunctionalTests extends BaseClass {

    // pagina web
    private static final String WEBSITE =
            "https://testare-manuala.locdejoacapentruitsti.com/blog/";

    // Scenariul de testare #1
    @Test
    public void verifyTitleIsCorrect() {
        // titlul de verificat
        final String TITLE = "My CMS – Practice website for testing sessions";

        // deschidem pagina web
        driver.get(WEBSITE);

        // verificam ca titlul este corect
        Assertions.assertEquals(TITLE,
                driver.getTitle(),
                "Title is not correct");
    }

    // Scenariul de testare #2
    @Test
    public void registerIsDisplayedAfterNavigationIsClickedForLowRez() throws InterruptedException {
        // redimensionam
        driver.manage().window().setSize(new Dimension(700, 1000));

        // deschidem pagina web
        driver.get(WEBSITE);

        // depistam elementele
        WebElement registerNavButton = driver.findElement(By.id("menu-item-73"));
        WebElement navigationButton = driver.findElement(By.id("main-navigation-toggle"));

        // verificam ca butonul este afisat pe ecran
        Assertions.assertTrue(navigationButton.isDisplayed(),
                "Navigation button is not displayed");

        // verificam ca textul butonului este cel corect
        Assertions.assertEquals("Navigation",
                navigationButton.getText(),
                "The Text is not 'Navigation'");

        // apasam pe buton
        navigationButton.click();

        Thread.sleep(2000); // "asteapta" pana register button is displayed

        // verificam ca un buton este de vizibil
        Assertions.assertTrue(registerNavButton.isDisplayed(),
                "Register Button is not displayed");

        // verificam ca textul butonului este cel corect
        Assertions.assertEquals("Register",
                registerNavButton.getText(),
                "The Text is not 'Register'");
    }
}
